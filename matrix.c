#include "matrix.h"
#include <string.h>
#include <math.h>

double *precision; /* Por padrao, MatrixSquare armazena um array de doubles */

MatrixSquare *matrix_square_create(int m) {
    int i;
    MatrixSquare *matrix;

    matrix = malloc(sizeof(MatrixSquare) + m * sizeof(precision));

    matrix->m = m;

    for (i = 0; i < m; ++i) 
        matrix->coefficients[i] = malloc(sizeof(*precision) * m);

    return matrix;
}

void matrix_square_destroy(MatrixSquare *matrix) {
    while (matrix->m)
        free(matrix->coefficients[--matrix->m]);

    free(matrix);
}

MatrixSquare *matrix_square_clone(MatrixSquare *matrix) {
    int i, j;
    MatrixSquare *copy = matrix_square_create(matrix->m);

    for (i = 0; i < matrix->m; ++i)
        for (j = 0; j < matrix->m; ++j)
            copy->coefficients[i][j] = matrix->coefficients[i][j];

    return copy;
}

/* --- */

char CHAR_BUF[LINESIZE];

void matrix_square_read(MatrixSquare *matrix) { /* Talvez usar strtof */
    char *token;
    int i, j;

    for (i = 0, j = 0; i < matrix->m; ++i, j = 0) {
        fgets(CHAR_BUF, LINESIZE, stdin);    

        token = strtok(CHAR_BUF, " ");

        do 
            matrix->coefficients[i][j++] = atof(token); 
        while ((token = strtok(NULL, " ")));

    }
}

/* --- */

void matrix_square_print(MatrixSquare *matrix) {
    int i, j;

    printf("%d\n", matrix->m);

    for (i = 0; i < matrix->m; ++i) {
        for (j = 0; j < matrix->m; ++j)
            printf("%6.6g ", matrix->coefficients[i][j]);
        putchar('\n');
    }

    putchar('\n');
}

/* ------------- */

int *identity_buf = NULL;

void pivot_set_buf(int m) {
    int i;

    if (identity_buf)
        free(identity_buf);

    identity_buf = malloc(sizeof(int) * m);

    for (i = 0; i < m; ++i)
        identity_buf[i] = i;
}

void pivot_swap_buf(int n, int pivot) {
    int aux;

    aux = identity_buf[n];
    identity_buf[n] = identity_buf[pivot];
    identity_buf[pivot] = aux;
}

void matrix_pivoting(MatrixSquare *matrix, int n) { /* Pivoteamento */
    int i, pivot = n;
    double *aux, max = 0;

    for (i = n; i < matrix->m; ++i) /* Maior coeficiente */
        if (fabs(matrix->coefficients[i][n]) > fabs(max)) { 
            max = matrix->coefficients[i][n];
            pivot = i;
        }

    pivot_swap_buf(n, pivot); 

    aux = matrix->coefficients[n];
    matrix->coefficients[n] = matrix->coefficients[pivot];
    matrix->coefficients[pivot] = aux; 
}

/* Recebe uma matriz com colunas trocadas   [a  ], [2, 0, 1]    [  a]
   e um buffer com a posicao das colunas    [ b ]            -> [b  ]
   "Ordena" as colunas                      [  c]               [ c ] */

void matrix_square_unpivot(MatrixSquare *matrix) { 
    int i, j;
    double coefficients_buf[matrix->m];

    for (i = 0; i < matrix->m; ++i) {
        for (j = 0; j < matrix->m; ++j) /* Armazena no buffer */
            coefficients_buf[identity_buf[j]] = matrix->coefficients[i][j];

        for (j = 0; j < matrix->m; ++j) /* Sobreescreve a matriz */
            matrix->coefficients[i][j] = coefficients_buf[j];
    }
    
}

/* ----------- */

void matrix_square_lu_decomp(MatrixSquare *matrix) { 
    int i, j, k; 
    double pivot, mult;

    pivot_set_buf(matrix->m); 

    for (i = 0; i < matrix->m; ++i) {
        matrix_pivoting(matrix, i);

        pivot = matrix->coefficients[i][i]; /* Diagonal Principal */

        for (k = i+1; k < matrix->m; ++k) { /* Zera coluna i */
            mult = matrix->coefficients[k][i]/pivot;

            for (j = i; j < matrix->m; ++j) /* Escalona linha k */
                matrix->coefficients[k][j] -= matrix->coefficients[i][j] * mult;

            matrix->coefficients[k][i] = mult; 
        }
    }
}

int solve_lu_system(MatrixSquare *matrix, double *z, double *df){ /* Quebrado... */
    int i, j;
    double coefficients_buf[matrix->m];

    /* Resolver [L][Z] = [DF] para encontrar [Z] */
    for (i = 0; i < matrix->m; ++i)
        z[i] = df[identity_buf[i]]; /* Despivoteia as var antes de resolver */

    for (i = 0; i < matrix->m; ++i) /* Linha i */ 
        for (j = 0; j < i; ++j)     /* Calcula Z_ik */
            z[i] -= z[j]*matrix->coefficients[i][j];

    /* Resolver [U][X] = [Z] para encontrar [X] */
    for (i = matrix->m - 1; i >= 0; --i) {
        for (j = matrix->m - 1; j > i; --j) 
            z[i] -= z[j]*matrix->coefficients[i][j];
        z[i] /= matrix->coefficients[i][i];
    }
    
    return 0;
}

/* ------------------------ */

MatrixSquare *matrix_square_lu_inverse(MatrixSquare *matrix) { /* Calculo da inversa */
    int i, j, k;
    double z[matrix->m];
    MatrixSquare *inverse  = matrix_square_create(matrix->m);

    for (k = 0; k < matrix->m; ++k) { /* Coluna K */

        /* Resolver [L][Z] = [C] para encontrar [Z] */
        for (i = 0; i < matrix->m; ++i)
            z[i] = 0;
        z[k] = 1;

        for (i = 0; i < matrix->m; ++i) /* Linha i */ 
            for (j = 0; j < i; ++j)     /* Calcula Z_ik */
                z[i] -= z[j]*matrix->coefficients[i][j];

        /* Resolver [U][X] = [Z] para encontrar [X] */
        for (i = matrix->m - 1; i >= 0; --i) {
            for (j = matrix->m - 1; j > i; --j) 
                z[i] -= z[j]*matrix->coefficients[i][j];
            z[i] /= matrix->coefficients[i][i];
            
            inverse->coefficients[i][k] = z[i];
        }
    }
    
    matrix_square_unpivot(inverse);

    return inverse;
}

/* --- */

double matrix_square_inverse_residue(MatrixSquare *matrix, MatrixSquare *inverse) { 
    int i, j, k;
    double aux, residue;
    MatrixSquare *tmp = matrix_square_create(matrix->m);

    /* Multiplicacao de matrix por inversa */
    for (k = 0; k < matrix->m; ++k)
        for (i = 0, aux = 0; i < matrix->m; ++i, aux = 0) {
            for (j = 0; j < matrix->m; ++j) {
                aux += matrix->coefficients[j][i] * inverse->coefficients[k][j];
            }
            tmp->coefficients[k][i] = aux;
        }

    /* Calculo do residuo */
    for (j = 0; j < matrix->m; ++j) {
        for (i = 0, aux = 0; i < matrix->m; ++i)
            aux += pow(tmp->coefficients[i][j] - (i == j ? 1 : 0), 2);
        residue += sqrt(aux);
    }

    matrix_square_destroy(tmp);

    return residue/matrix->m;
}

/*********************************************************/

void parcial_swap(MatrixSquare *gauss,double *v, int *reswap,int l,int l_pivo){
    double t, *pt;
    int n;

    pt = gauss->coefficients[l];
    gauss->coefficients[l] = gauss->coefficients[l_pivo];
    gauss->coefficients[l_pivo] = pt;

    t = v[l];
    v[l] = v[l_pivo];
    v[l_pivo] = t;

    n = reswap[l];
    reswap[l] = reswap[l_pivo];
    reswap[l_pivo] = n;
}

void parcial_reswap(MatrixSquare *gauss,double **v, int *reswap){
    double *t = malloc(sizeof(double)*gauss->m);
    for (int line = 0; line < gauss->m; line++)
        t[reswap[line]] = (*v)[line];
    
    for (int line = 0; line < gauss->m; line++)
        (*v)[line] = t[line];

    free(t);

}

/*
*   Faz decomposição LU
*   MatrixSquare *m     Ponteiro para Matrix
*   MatrixSquare *i     Ponteiro para Matrix identidade de tamanho m->m
*   Retorna       Uma matriz que contem os elementos da matriz LU
*                 nos elementos abaixo da diagonal estão os elementos 
*                 da matriz L( com exceção dos 1 da diagonal) 
*                 e na diagonal para cima os elementos da Matriz U]
*                 em erro retorna NULL
*/ 
MatrixSquare* gauss_parcial(MatrixSquare *m, double *v, int *reswap){
    /* Variaveis */
	double pivo = 0, multi;
	int l_pivo = 0;
    
    MatrixSquare *lu = matrix_square_clone(m);
    if(lu ==  NULL)
	    return NULL;
    
    /* Percorre Coluna */
    for (int col = 0; col < m->m; col++){
        /* Acha pivo */
        for (int lin = col; lin < m->m; lin++){
            if (fabs(pivo) <= fabs(lu->coefficients[lin][col]) ){
                pivo = lu->coefficients[lin][col];
                l_pivo = lin;
            }
        }

	    /* Faz o Pivoteamento */
	    if (l_pivo != col)
		    parcial_swap(lu,v,reswap,col,l_pivo);
	    
        for (int lin = col+1; lin < m->m; lin++){
            multi = lu->coefficients[lin][col]/pivo;
            //lu->coefficients[lin][col] = multi;
            lu->coefficients[lin][col] = 0;
            for (int i = col+1; i < m->m; i++){
                lu->coefficients[lin][i] -= lu->coefficients[col][i]*multi ;
            }
            v[lin] -= v[col]*multi;
		}
		pivo = 0;
    }
    return lu;
}

int solve_system(MatrixSquare *a, double *b, double *c){
    int *reswap = malloc(sizeof(int) * a->m);
    for (int i = 0; i < a->m; i++)
        reswap[i] = i;

    MatrixSquare *g = gauss_parcial(a,c,reswap);
    
    /* Deixa apenas a diagonal  */
    for (int col = a->m-1; col >= 0; col--){
        b[col] = c[col]/g->coefficients[col][col];
        for (int lin = col-1; lin >= 0; lin--){
            c[lin] -= g->coefficients[lin][col] * b[col];
            g->coefficients[lin][col] = 0;
        }
    }
    
    matrix_square_destroy(g);
    free(reswap);
    //parcial_reswap(g,b,reswap);

    return 0;
}
