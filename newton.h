#ifndef NEWTON_H
#define NEWTON_H

#include <matheval.h>
#include "matrix.h"

typedef struct {
    int m;
    void ***functions;
} MatrixFunction;

MatrixFunction *matrix_function_create(int m) ;

void matrix_function_destroy(MatrixFunction *matrix) ;


/*
*   Cria uma Hessiana com as funçoes 
*/
MatrixFunction *matrix_function_hessian_create(void **gradient, int m, char **names) ;
/*
*   Calcula os resultados de uma MatrixFunction recebendo  X variaveis 
*   
*/
void matrix_evaluate(MatrixFunction *func, MatrixSquare *result, char **names, double *values) ;


/*
*   Faz o metodo de quasi-Newton DFP
*   void **gradient         Evaluator do vetor gradiente
*   MatrixSquare *H         Matriz Hessiana
*   char **names            Nome de todas as variaveis
*   double epsilon          Epsilon de erro
*   int max                 Maximo de iteração
*/
int quasi_newton_dfp(void **gradient, MatrixSquare *h, double **X,char **names, double epsilon,int max);

/*
*   Faz o metodo de Newton modificado
*   void **gradient         Evaluator do vetor gradiente
*   MatrixFunction *H       Matriz Hessiana de funções
*   char **names            Nome de todas as variaveis
*   double epsilon          Epsilon de erro
*   int max                 Maximo de iteração
*   int hess_step           Intervalo entre calculo da hessiana 
*/
int newton_modificado(void **gradient, MatrixFunction *H, double **X,char **names, double epsilon,int max, int hess_step);



#endif

