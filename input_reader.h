#ifndef INPUT_READER_H_
#define INPUT_READER_H_

/*
* Le de standard input e forma a função da Instancia
*   int *n              Endereço de n
*   int *k              Endereço de k
*   double *e           Endereço de epsilon
*   int *m              Endereço de max_iteração
*   int *hs             Endereço de hess_step
*   double ***X         Endereço da matriz dos valores das iteraçoes de X com x[0] preenchido
*   char ***var_name    Endereço do array de strings com o nomes das variaveis
*   char **f_buffer     Endereço da String da função da instancia
*/
int input(int *n, int *k, double *e, int *m, int *hs,double ***X, char ***var_name,char **f_buffer );

#endif
