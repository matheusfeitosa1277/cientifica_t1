#include "matrix.h"
#include "newton.h"

#include <matheval.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>


MatrixFunction *matrix_function_create(int m) {
    MatrixFunction *matrix;

    matrix = malloc(sizeof(MatrixFunction));
    matrix->m = m;
    matrix->functions = malloc(sizeof(void*) * m);

    for (int i = 0; i < m; ++i)
        matrix->functions[i] = malloc(sizeof(void*) * m);

    return matrix;
}

void matrix_function_destroy(MatrixFunction *matrix) {
    int i, j;

    for (i = 0; i < matrix->m; ++i) {
        for (j = 0; j < matrix->m; ++j)
            evaluator_destroy(matrix->functions[i][j]);
        free(matrix->functions[i]);
    }

    free(matrix->functions);

    free(matrix);
}

MatrixFunction *matrix_function_hessian_create(void **gradient, int m, char **names) {
    int i, j;

    MatrixFunction *hessian = matrix_function_create(m);

    // Calculo da Jacobiana do gradiente (Hessiana)
    for (i = 0; i < m; ++i)
        for (j = 0; j < m; ++j) 
            hessian->functions[i][j] = evaluator_derivative(gradient[i], names[j]);

    return hessian;
}

void matrix_evaluate(MatrixFunction *func, MatrixSquare *result, char **names, double *values) {
    int i, j;

    for (i = 0; i < result->m; ++i)
        for (j = 0; j < result->m; ++j)
            result->coefficients[i][j] = evaluator_evaluate(func->functions[i][j], result->m, names, values);
}

/*******************************************************************************************/
int newton_modificado(void **gradient, MatrixFunction *H, double **X,char **names, double epsilon,int max, int hess_step){
    MatrixSquare *h = matrix_square_create(H->m); 
    double *delta = malloc(sizeof(double)*h->m);
    double *df = malloc(sizeof(double)*h->m);
    int i, j, flag = 1;

    for (i = 0; i < max-1; i++){

        /* Calcula -df */
        for (j = 0; j < h->m; j++)
            df[j] = -1.0*evaluator_evaluate(gradient[j],h->m,names,X[i]);
        
        /* Check Gradiente */
        flag = 1;
        for (j = 0; j < h->m && flag; j++){
            if (fabs(df[j]) > epsilon)
                flag = 0;
        }
        if (flag == 1){
            free(delta);
            free(df);
            matrix_square_destroy(h);
            return i;
        }

        /* Calcula Hessiana */
        if (i%hess_step == 0) {
            matrix_evaluate(H, h, names, X[i]);
            matrix_square_lu_decomp(h);
        }

        
        /* Resolve Sistema Linear*/
        solve_lu_system(h,delta,df);
        //solve_system(h,delta,df);

        /* Calcula X[i+1] */
        for (j = 0; j < h->m; j++)
            X[i+1][j] = X[i][j] + delta[j];
        

        /* Check Delta */
        flag = 1;
        for (j = 0; j < h->m  && flag; j++){
            if (fabs(delta[j]) > epsilon)
                flag = 0;
        }
        if (flag == 1){
            free(delta);
            free(df);
            matrix_square_destroy(h);
            return i+1;
        }
        
    }
    free(delta);
    free(df);
    matrix_square_destroy(h);
    return max;
}

/*
*   Faz uma multiplicação de (aT)b , sendo a e b vetores de tamanho n,
*   utiliza soma de Kahan para diminuir cancelamento catastrofico
*   
*   double *a       Vetor transposto
*   double *n       Vetor 
*   int n           Tamanho dos vetores   
*/
double kahan_vector(double *a, double *b, int n){
    /* s = soma | e := erro | x := soma c/ erro | y := valor corrigido*/
    double s = 0.0, e = 0.0, x, y;
    for (int i = 0; i < n; i++){
        y = (a[i] * b[i]) - e;
        x = s + y;
        e = (x - s) - y; 
        s = x;
    }
    return s;
}

int quasi_newton_dfp_hessiana(MatrixSquare *h, void **gradient, double **X, char** names,int k){
    /* Variaveis para conta */
    double *p = malloc(sizeof(double)*h->m); 
    double *q = malloc(sizeof(double)*h->m); 
    double *t = malloc(sizeof(double)*h->m); 
    MatrixSquare *m1 = matrix_square_create(h->m);
    MatrixSquare *m2 = matrix_square_create(h->m);
    double d1, d2;

    /*            m1          d1            m2               d2     */
    /* H = H + (p*(p)T) / ((pT)*q) - (((H*q)*(qT))*H) / (((q)T*H)q) */
    
    /* p = x[k+1]-x[k]*/
    for (int i = 0; i < h->m; i++){
        p[i] = X[k+1][i] - X[k][i];
    }

    /* q = gf([k+1])-gf(x[k])*/
    for (int i = 0; i < h->m; i++){
        q[i] = evaluator_evaluate(gradient[i],h->m,names,X[k+1]) - evaluator_evaluate(gradient[i],h->m,names,X[k]);
    }

    /****************************************/
    /* Calculando m2 */

    /* t = (H*q)     resulta num vetor */
    for (int i = 0; i < h->m; i++){
        for (int j = 0; j < h->m; j++){
           t[i] =  h->coefficients[i][j] * q[j];
        }
    }      

    /* m1 = (H*q)*(qT) */
    /* m1 = (t)*(qT)   Resulta em uma matriz*/
    for (int i = 0; i < m1->m; i++){
        for (int j = 0; j < m1->m; j++){
            m1->coefficients[i][j] = t[i]*q[j];
        }
    }

    /* m2 = (H*q)*(qT)*(H) */
    /* m2 = (m1)*(H) */
    for (int i = 0; i < h->m; i++){
        for(int j = 0; j < m1->m; j++){
            m2->coefficients[i][j] = 0;
            for (int k = 0; k < h->m; k++){
               m2->coefficients[i][j] += m1->coefficients[i][k] * h->coefficients[k][j];
            }
        }
    }

    /****************************************/
    /* Calculando d2 */

    /* t = (qT)*H */
    for (int i = 0; i < h->m; i++){
        t[i] = 0;
        for (int j = 0; j < h->m; j++){
           t[i] +=  h->coefficients[j][i] * q[j];
        }
    }     
    /* d2 = ((qT)*H) * q */
    /* d2 =  (tT) * q)   */
    d2 = kahan_vector(t,q,m1->m);
    
    //d2 = 0;
    //for (int j = 0; j < h->m; j++){
    //    d2 +=  t[j] * q[j];
    //}

    /****************************************/
    /* Calculando m1 */

    /* m1 = (p*(p)T) */
    for (int i = 0; i < m1->m; i++){
        for (int j = 0; j < m1->m; j++){
            m1->coefficients[i][j] = p[i]*p[j];
        }
    }

    /****************************************/
    /* Calculando d1 */
    /* d1 = ((pT)*q) */
    d1 = kahan_vector(p,q,m1->m);
    
    //d1 = 0;
    //for (int i = 0; i < m1->m; i++){
    //    d1 += p[i]*q[i];
    //}

    /****************************************/
    /* Calculando  H = H + (p*(p)T) / ((pT)*q) - (((H*q)*(qT))*H) / (((q)T*H)q) */
    /* Calculando  H = H + m1/d1 -m2/d2 */

    for (int i = 0; i < h->m; i++){
        for (int j = 0; j < h->m; j++){
            h->coefficients[i][j] += m1->coefficients[i][j]/d1 - m2->coefficients[i][j]/d2;
        }
    }
    free(p);
    free(q);
    free(t);
    matrix_square_destroy(m1);
    matrix_square_destroy(m2);
    return 0;
}


/*
*   Faz o metodo de quasi-Newton DFP
*   void **gradient         Evaluator do vetor gradiente
*   MatrixFunction *H       Matriz Hessiana de funções
*   char **names            Nome de todas as variaveis?
*   double epsilon          Epsilon de erro
*   int max                 Maximo de iteração
*/
int quasi_newton_dfp(void **gradient, MatrixSquare *h, double **X,char **names, double epsilon,int max){
    double *delta = malloc(sizeof(double)*h->m);
    double *df = malloc(sizeof(double)*h->m);
    int j = 0, i,flag = 1;
    for (i = 0; i < max -1; i++){

        /* Calcula -df */
        for (j = 0; j < h->m; j++)
            df[j] = -1.0*evaluator_evaluate(gradient[j],h->m,names,X[i]);

        /* Verifica gradiente == 0 */
        flag = 1;
        for (j = 0; j < h->m && flag; j++){
            if (fabs(df[j]) > epsilon)
               flag = 0;
        }
        if (flag == 1){
            free(delta);
            free(df);
            return i;
        }
        /* Resolve sistema */
        solve_system(h,delta,df);

        /* Calcula X[i+1] */
        for (j = 0; j < h->m; j++)
            X[i+1][j] = X[i][j] + delta[j];

        /* Verifica erro no Sistema */
        flag = 1;
        for (j = 0; j < h->m && flag; j++){
            if (fabs(delta[j]) > epsilon)
               flag = 0;
        }
        if (flag == 1){
            free(delta);
            free(df);
            return i+1;
        }
        /* Calcula a hessiana H[i+1] */
        
        quasi_newton_dfp_hessiana(h,gradient,X,names,i);

    }
    free(delta);
    free(df);
    return max;
}
