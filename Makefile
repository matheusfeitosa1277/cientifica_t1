CC=gcc
CFLAG=-Wall
LFLAG= -lm -I/usr/include -L/usr/lib -lmatheval
NAME=pontosCriticos 

all:$(NAME)

$(NAME): main.o input_reader.o newton.o matrix.o
	$(CC) $(CFLAG) newton.o matrix.o main.o input_reader.o  $(LFLAG) -o $(NAME)  

main.o: main.c input_reader.h newton.h
	$(CC) -c $(CFLAG) main.c $(LFLAG)  

newton.o: newton.c matrix.h newton.h
	$(CC) -c $(CFLAG) newton.c $(LFLAG)  

matrix.o: matrix.c matrix.h
	$(CC) -c $(CFLAG) matrix.c $(LFLAG)  
	
input_reader.o: input_reader.h input_reader.c
	$(CC) -c $(CFLAG) input_reader.c $(LFLAG)  

matrix_operation.o: matrix_operation.c matrix_operation.h
	$(CC) -c $(CFLAG) matrix_operation.c $(LFLAG)  


clean: 
	rm -f *.o

purge: clean
	rm $(NAME)
