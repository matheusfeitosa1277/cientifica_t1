#ifndef MATRIX_H
#define MATRIX_H

#define LINESIZE 1024

#include <stdio.h> 
#include <stdlib.h> 

/* Implementar um programa para calcular a matriz inversa de matrizes quadradas 
    usando o Método da Fatoração LU com pivoteamento parcial. */

typedef struct { /* Matrix A_{m, n}, m == n */
    int m;
    double *coefficients[0];
} MatrixSquare;

MatrixSquare *matrix_square_create(int m);

void matrix_square_destroy(MatrixSquare *matrix);

MatrixSquare *matrix_square_clone(MatrixSquare *matrix);

/* --- */

/* Recebe uma sequencia de M streams da stdin, composta de doubles separados por ' '
   Salva esses valores nos coefficientes da matriz */

void matrix_square_read(MatrixSquare *matrix);

/* --- */

/*  Imprime o valor M da matriz. Na proxima linha
    Imprime, em grid MxM, os coefficientes da matriz */


void matrix_square_print(MatrixSquare *matrix);


/* void pivot_set_buf(); */
/* void pivot_swap_buf(); */

/* void matrix_pivoting(MatrixSquare *matrix, int n); */
/* void matrix_square_unpivot(MatrixSquare *matrix); */



/* Recebe uma matriz A qualquer             [a b c]    [U U U]
   Modifica os coeficientes no intuito de   [d e f] -> [L U U] 
   decompor a mesma em Upper e Lower        [g h i]    [L L U] */

void matrix_square_lu_decomp(MatrixSquare *matrix);

/* Recebe uma matriz A no formato LU      [U U U]          
                                          [L U U]  -> return (A'1)  
   Retorna a inversa de A                 [L L U]                   */

MatrixSquare *matrix_square_lu_inverse(MatrixSquare *matrix); /*???*/

/* --- */

double matrix_square_inverse_residue(MatrixSquare *matrix, MatrixSquare *inverse);

int solve_system(MatrixSquare *a, double *b, double *c);

int solve_lu_system(MatrixSquare *matrix, double *z, double *df);

#endif

