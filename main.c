#include "newton.h"
#include "input_reader.h"
#include <stdio.h>

int main() {
    int n, k, m, hs;
    double e, **X;
    char **names, *func_string;
    int i;

    /* Entrada */
    input(&n, &k, &e, &m, &hs, &X, &names, &func_string);

    /* --------------------------- */

    void *function = evaluator_create(func_string);
    void **gradient = malloc(sizeof(void*) * n);

    for (i = 0; i < n; ++i)
        gradient[i] = evaluator_derivative(function, names[i]);

    MatrixFunction *hessian = matrix_function_hessian_create(gradient, n, names);
    MatrixSquare *result = matrix_square_create(n);

    matrix_evaluate(hessian, result, names, X[0]);


    /* Newton */
    int iter = newton_modificado(gradient, hessian, X, names, e,m, hs);
    for (int i = 0; i < iter; i++){
        printf("Iteração %i ::\n",i);
        printf("F(X) = %lf\n\n", evaluator_evaluate(function,n,names,X[i]));
        for (int j = 0; j < n; j++){
            printf("gradiente :: %lf\n", evaluator_evaluate(gradient[j],n,names,X[i]));
            printf("X         :: %lf\n", X[i][j]);
        }
    }

    matrix_square_destroy(result);
    evaluator_destroy(function);
    matrix_function_destroy(hessian);
    for (i = 0; i < n; ++i)
        evaluator_destroy(gradient[i]);

    /* ---------------------- */
    for (i = 0; i < m; ++i)
        free(X[i]);

    for (i = 0; i < n; ++i)
        free(names[i]);

    free(X);
    free(names);
    free(func_string);
    free(gradient);

    return 0;
}

