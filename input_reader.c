#include "input_reader.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_F_BUFFER 1024
#define MAX_T_BUFFER 1024
/*
* Le de standard input e forma a função da Instancia
*   int *n              Endereço de n
*   int *k              Endereço de k
*   double *e           Endereço de epsilon
*   int *m              Endereço de max_iteração
*   int *hs             Endereço de hess_step
*   double ***X         Endereço da matriz dos valores das iteraçoes de X com x[0] preenchido
*   char ***var_name    Endereço do array de strings com o nomes das variaveis
*   char **f_buffer     Endereço da String da função da instancia
*/
int input(int *n, int *k, double *e, int *m, int *hs,double ***X, char ***var_name,char **f_buffer ){
    double x;
    char *t_buffer;
    if(scanf("%i %i %lf %lf %i %i", n,k,&x,e,m,hs) != 6)
        return 1;

    /* X[Iteração][numero da variavel]*/
    *X = malloc(sizeof(double*) * (*m) );
    if(*X == NULL)
        return 1;



    for(int i = 0 ; i < (*m); i++){
        (*X)[i] = malloc(sizeof(double) * (*n) );
        if((*X)[i] == NULL)
            return 1;
    }
   
    /* Aproximação inicial */
    for(int j = 0; j < (*n); j++){
        (*X)[0][j] = (x);
    }
    /* Nomes de Variaveis */
    *var_name = malloc(sizeof(char*) * (*n));
    if(var_name == NULL)
        return 1;

    /* Usar piso de log de n para alocar o menor espaço possivel */
    for(int i = 0 ; i < (*n); i++){
        (*var_name)[i] = malloc(sizeof(char) * ((*n)+1));  
        if((*var_name) == NULL)
            return 1;
        snprintf((*var_name)[i],sizeof(char) * ((*n)+1),"x%i",i+1);
    }

    /* Buffer da Função */
    (*f_buffer) = malloc(sizeof(char) * MAX_F_BUFFER);
    if((*f_buffer) == NULL)
        return 1;
    (*f_buffer)[0] = '\0';

    t_buffer = malloc(sizeof(char) * MAX_T_BUFFER);
    if(t_buffer == NULL)
        return 1;

    //return 0;
    /* Primeiro Somatorio */
    int i,j;
    for(i = 1; i <= (*n)-(*k)/2; i++){
        snprintf(t_buffer,sizeof(char) * MAX_T_BUFFER, "(x%i -(",i);
        strcat((*f_buffer),t_buffer);
        /* Segundo Somatorio */
        for(j = 1; j <= (*k)/2-1 ; j++){
            snprintf(t_buffer,sizeof(char) * MAX_T_BUFFER, "(x%i)^%i + ",i+j,j);
            strcat((*f_buffer),t_buffer);
        }
        snprintf(t_buffer,sizeof(char) * MAX_T_BUFFER, "(x%i)^%i))^2+",i+j,j);
   	strcat((*f_buffer),t_buffer);
    }
    (*f_buffer)[strlen(*f_buffer)-1] = '\0';
    free(t_buffer);


    return 0;
}
